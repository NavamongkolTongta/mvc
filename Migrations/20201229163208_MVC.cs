﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVC.Migrations
{
    public partial class MVC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    CountryId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryName = table.Column<string>(maxLength: 100, nullable: true),
                    FormalName = table.Column<string>(maxLength: 100, nullable: true),
                    IsoAlpha3CodeD1 = table.Column<string>(maxLength: 100, nullable: true),
                    IsonumericCode = table.Column<long>(nullable: false),
                    CountryType = table.Column<string>(maxLength: 100, nullable: true),
                    Continent = table.Column<string>(maxLength: 100, nullable: true),
                    Region = table.Column<string>(maxLength: 100, nullable: true),
                    Subregion = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.CountryId);
                });

            migrationBuilder.CreateTable(
                name: "Stateprovinces",
                columns: table => new
                {
                    StateprovinceId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StateprovinceCode = table.Column<string>(nullable: true),
                    StateprovinceName = table.Column<string>(maxLength: 100, nullable: true),
                    CountryId = table.Column<long>(nullable: false),
                    SalesTerritoryE1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stateprovinces", x => x.StateprovinceId);
                    table.ForeignKey(
                        name: "FK_Stateprovinces_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    CityId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CityName = table.Column<string>(maxLength: 100, nullable: true),
                    StateprovinceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.CityId);
                    table.ForeignKey(
                        name: "FK_Cities_Stateprovinces_StateprovinceId",
                        column: x => x.StateprovinceId,
                        principalTable: "Stateprovinces",
                        principalColumn: "StateprovinceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cities_StateprovinceId",
                table: "Cities",
                column: "StateprovinceId");

            migrationBuilder.CreateIndex(
                name: "IX_Stateprovinces_CountryId",
                table: "Stateprovinces",
                column: "CountryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Stateprovinces");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
