using System;
using Microsoft.AspNetCore.Mvc;
using MVC.Data;
using System.Linq;
using MVC.Models.DataModels;
using MVC.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MVC.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlaceController : BaseController
    {
        public PlaceController(ApplicationDbContext db) : base(db) { }

        // [HttpPost("Query1")]
        // public IActionResult Query1(Country model)
        // {
        //     //Console.WriteLine(model.id);
        //     //Console.WriteLine(model.IsonumericCode);
        //     _db.Country.Add(model);
        //     _db.SaveChanges();
        //     return Ok(model);
        // }
        [HttpGet("Query1")]
        public IActionResult Query1(){
            var name = _db.Country.Where(it => it.Continent == "Asia")
                          .Select(it => it.CountryName);
            return Json(name);
        }
        [HttpGet("Query2")]
        public IActionResult Query2(){
            var country = _db.Country.Select(it => it.CountryName)
                                     .Distinct();
            return Json(country);
        }
        [HttpGet("Query3")]
        public IActionResult Query3(){
            var highest = _db.Country.Select(it => it.IsoAlpha3CodeD1).Max();
            return Json(highest);
        }
        [HttpGet("Query4")]
        public IActionResult Query4(){
            var top10 = _db.Country.Select(it => it.IsoAlpha3CodeD1)
                                     .OrderByDescending(it => it)
                                     .Take(10);
            return Json(top10);
        }
        [HttpGet("Query5")]
        public IActionResult Query5(){
            var name = _db.Stateprovince.Include(x => x.Country)
                                        .Select(x => new {x.StateprovinceName, x.Country.CountryName});
            return Json(name);
        }
        // [HttpGet("Query6")]
        // public IActionResult Query6(){
        //     var name = 
        // }
        
    }
}