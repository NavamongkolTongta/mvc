using MVC.Data;
using MVC.Models;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;

namespace MVC.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly ApplicationDbContext _db;

        public BaseController(ApplicationDbContext _db)
        {
            this._db = _db;
        }

    }
}