using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Models.DataModels

{
    public class Stateprovince
    {
        public long StateprovinceId { get; set; }
        public string StateprovinceCode { get; set; }
        [StringLength (100)]
        public string StateprovinceName { get; set; }
        public long CountryId { get; set; }
        public string SalesTerritoryE1 { get; set; }
        [ForeignKey(nameof(CountryId))]
        public virtual Country Country { get; set; }
    } 
}