using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Models.DataModels

{
    public class City
    {
        public long CityId { get; set; }
        [StringLength (100)]
        public string CityName { get; set; }
        public long StateprovinceId { get; set; }
        [ForeignKey(nameof(StateprovinceId))]
        public virtual Stateprovince Stateprovince{ get; set; }
    }

}