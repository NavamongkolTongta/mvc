using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC.Models.DataModels
{
    public class Country
    {
        public long CountryId { get; set; }
        [StringLength (100)]
        public string CountryName { get; set; }
        [StringLength (100)]
        public string FormalName { get; set; }
        [StringLength(100)]
        public string IsoAlpha3CodeD1 { get; set; }
        public long IsonumericCode { get; set; }
        [StringLength (100)]
        public string CountryType { get; set; }
        [StringLength (100)]
        public string Continent { get; set; }
        [StringLength (100)]
        public string Region { get; set; }
        [StringLength (100)]
        public string Subregion { get; set; }
        // [ForeignKey(nameof())]
        // public virtual City City { get; set; }
    }
}

