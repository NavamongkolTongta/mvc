// using System;
// using System.ComponentModel.DataAnnotations;

// namespace MVC.Models
// {
//     public class UserTimeStamp : Entity
//     {
//         public DateTime CreatedAt { get; set; }
//         [StringLength(200)]
//         public string CreatedBy { get; set; }
//         public DateTime UpdatedAt { get; set; }
//         [StringLength(200)]
//         public string UpdatedBy { get; set; }
//         public bool IsActive { get; set; }
//         public override void OnBeforeInsert(DateTime dt)
//         {
//             this.CreatedAt = dt;
//             this.UpdatedAt = dt;
//             this.CreatedBy = "ST";
//             this.UpdatedBy = "ST";
//             this.IsActive = true;
//         }

//         public override void OnBeforeUpdate(DateTime dt)
//         {
//             this.UpdatedAt = dt;
//             this.UpdatedBy = "ST";
//         }
//     }
// }